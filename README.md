# demo-uis
A scene for demonstrating approaches to, and issues with, working with multiple 2D UIs in Decentraland SDK7.

## Background: I needed a way to manage multiple unrelated 2D Uis being added and removed independently.
Why?
1. Because one can only call ReactEcsRenderer once in a scene, 
2. Because my scenes sometimes need more than one 2D Ui
3. Because those Uis may be completely unrelated, in their own reusable modules, for example
   a. BuilderHUD
   b. CinemaControls
   c. AdmissionTicket
   d. Dance button
   e. Stage Manager Controls

This scene demonstrates such a mechanism, 
but also demonstrates some issues with SDK7 2D UIs, which I’ll report in decentraland/sdk,
including interfering with (cross-talking with) the properties of other UiEntities.


## Issues that this scene can help demonstrate are:

### SDK7: The first mouse click” on a 3D object that has an OnPointerDown handler, will not have an effect, requiring another click.
https://github.com/decentraland/sdk/issues/936


### SDK7: The first “click” on a 2D UI that appears when a scene first loads will not have an effect, requiring another click.
https://github.com/decentraland/sdk/issues/937



### SDK7: Removing one UiEntity can damage other remaining UiEntities
https://github.com/decentraland/sdk/issues/938


### SDK7: Removing a temporary message display can cause another ui element to move
https://github.com/decentraland/sdk/issues/939


### SDK7: Unless the outer UiEntity / parent of a temporary 2D UI is full screen, then it doesn’t appear on the screen
https://github.com/decentraland/sdk/issues/940

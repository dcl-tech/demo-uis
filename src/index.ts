// ///////////////////////////////////////////////////////////////////////////
// This scene demonstrates some issues with the 2D UI support in SDK7
// (c) 2023 by Carl Fravel
// Permission is given to DCL Foundation to use the files in
// this scene but only for debugging UI issues and testing of fixes.
// Any other usage, by Decentraland or anyone else, requires written permission from Carl Fravel.
//
// Special notes in these files are indicated by !CF
// ///////////////////////////////////////////////////////////////////////////

// Background: I needed a way to manage multiple unrelated 2D Uis being added and removed independently.
// Why?
// 1. Because one can only call ReactEcsRenderer once in a scene, 
// 2. Because my scenes sometimes need more than one 2D Ui
// 3. Because those Uis may be completely unrelated, in their own reusable modules, for example
//    a. BuilderHUD
//    b. CinemaControls
//    c. AdmissionTicket
//    d. Dance button
//    e. Stage Manager Controls
// This scene demonstrates such a mechanism, 
// but also demonstrates some issues with SDK7 2D UIs, which I’ll report in decentraland/sdk, 
// including interfering with (crosstalking with) the properties of other UiEntities.
// 
// There are two configuration variables in this project, which may need changing depending
// on the issue being demonstrated.
// 1. In this file, autoLoadUis controls whether the 2D Uis appear on scene load, 
// or need to be instantiated by clicking on 3D objects in the scene
// 2. In the src/modules/DemoUis.tsx file, there is another configuration variable
// that controls how multipe-UIs are composited for rendering.

export * from '@dcl/sdk'

import {Color4} from '@dcl/sdk/math'
import {
    engine,
    Entity,
    Transform,
    MeshRenderer,
    MeshCollider,
    Material,
    pointerEventsSystem,
    InputAction
} from '@dcl/sdk/ecs'

import {
    showUi_1, hideUi_1, 
    showUi_2, hideUi_2, 
    showUi_3, hideUi_3} from './modules/DemoUis'

///////////////////////////////
// Tests configuration
// (also one in DemoUis.tsx)
////////////////////////////////

let autoLoadUis:boolean = false // Check this in with false


////////////////////////////////
// Main
///////////////////////////////
export function main () {
    if (autoLoadUis) {
        showUi_1() // !CF show Ui_1 before Ui_2 to see the issue of impact of 1 on the "next" of Ui_2 or Ui_3
        showUi_2() // !CF move this to 3rd place to see that Ui_1's impact is only on the "next" of Ui_2 or Ui_3
        showUi_3()
    }
    else {
        // set up a button to show / re-show Uis
        let showUi_1_Button:Entity = engine.addEntity()
        Transform.create(showUi_1_Button, {
            position: {
                x: 4,
                y: 1,
                z: 6
            }
        })
        MeshRenderer.setBox(showUi_1_Button)
        MeshCollider.setBox(showUi_1_Button)
        Material.setBasicMaterial(showUi_1_Button,{
            diffuseColor:Color4.Green()
        })

        pointerEventsSystem.onPointerDown({
            entity: showUi_1_Button,
            opts: {
                button: InputAction.IA_POINTER,
                hoverText: 'Show UIs',
                maxDistance: 32,
                showFeedback: true

            }
        }, () => {
            showUi_1() // !CF show Ui_1 before Ui_2 to see the issue of impact of 1 on the "next" of Ui_2 or Ui_3
            showUi_2() // !CF move this to 3rd place to see that Ui_1's impact is only on the "next" of Ui_2 or Ui_3
            showUi_3()
        
        })

        // set up a box as a button to reset the scene
        let reset_Uis_Button:Entity = engine.addEntity()
        Transform.create(reset_Uis_Button, {
            position: {
                x: 6,
                y: 1,
                z: 6
            }
        })
        MeshRenderer.setBox(reset_Uis_Button)
        MeshCollider.setBox(reset_Uis_Button)
        Material.setBasicMaterial(reset_Uis_Button,{
            diffuseColor:Color4.Black()
        })
        pointerEventsSystem.onPointerDown({
            entity: reset_Uis_Button,
            opts: {
                button: InputAction.IA_POINTER,
                hoverText: 'Reset Scene',
                maxDistance: 32,
                showFeedback: true

            }
        }, () => {
            hideUi_1()
            hideUi_2()
            hideUi_3()
        })
    }
}





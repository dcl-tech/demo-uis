//////////////////////////////////////////////////////////////////////////
// DemoUis
// Two Uis that demonstrate a bug consisting of crosstalk between them.
// (c) 2023 by Carl Fravel
// Permission is given to DCL Foundation to use this scene for issue resolution
//////////////////////////////////////////////////////////////////////////


import {
    Color4,
} from '@dcl/sdk/math'
  
import { 
    engine,
    UiCanvasInformation
} from '@dcl/sdk/ecs'
import ReactEcs, { UiEntity, Button, Label, ReactEcsRenderer } from '@dcl/sdk/react-ecs'
import * as utils from "@dcl-sdk/utils"

let debug = false

///////////////////////////////
// Tests configuration
// (also another one in index.tsx)
//
// useGeneralizedCompositing controls which method of Ui compositing is used:
// true shows the issue of closing Ui_1 impacting the appearance of Ui_2
let useGeneralizedCompositing:boolean = true // check this in with true


///////////////////////////////////////////
// Ui_1 is an example of a responsive UI
////////////////////////////////////////////
let ui_1_id:number = 0
let ui_1_initialized = false 

function renderUi_1() {
    let canvas = UiCanvasInformation.get(engine.RootEntity)
    let TICKET_WIDTH = canvas.width * 0.4 // fill 2/3 of the screen width
    let TICKET_HEIGHT = TICKET_WIDTH * 9 / 16 // 16:9 aspect ratio
    let TICKET_POS_X = (canvas.width*0.9-TICKET_WIDTH) // 200 // positive to right
    let TICKET_POS_Y = (canvas.height-TICKET_HEIGHT) / 2 // 150 // positive up

    let CAPTION_WIDTH = 0 // TICKET_WIDTH * 0.5 // fill 2/3 of the screen width
    let CAPTION_HEIGHT = 0 // CAPTION_WIDTH * 0.1 // 16:9 aspect ratio
    let CAPTION_X = (TICKET_WIDTH-CAPTION_WIDTH) / 2  // 200 // positive to left
    let CAPTION_Y = TICKET_HEIGHT * 0.05 // (TICKET_HEIGHT-CAPTION_HEIGHT) / 2 // from top
    let CAPTION_FONT_SIZE = 32 * canvas.width/1280
    
    let INSTRUCTIONS_WIDTH = 0 //TICKET_WIDTH * 0.55 // fill 2/3 of the screen width
    let INSTRUCTIONS_HEIGHT = 0 // TICKET_HEIGHT  /6
    let INSTRUCTIONS_X = (TICKET_WIDTH-INSTRUCTIONS_WIDTH) / 2  // 200 // positive to left
    let INSTRUCTIONS_Y = TICKET_HEIGHT * 0.22 // (TICKET_HEIGHT-INSTRUCTIONS_HEIGHT) / 2 // from top, positive down
    let INSTRUCTIONS_FONT_SIZE = 24 * canvas.width/1280

    let BUTTON_WIDTH = TICKET_WIDTH * 0.3
    let BUTTON_HEIGHT = BUTTON_WIDTH * 0.25
    let SUBMIT_BUTTON_X = TICKET_WIDTH/2 - BUTTON_WIDTH/2
    let BUTTON_Y = TICKET_HEIGHT * 0.02 
    let BUTTON_FONT_SIZE = 24 * canvas.width/1280

    return <UiEntity // Ticket
        uiTransform={{
            positionType: 'absolute',
            width: TICKET_WIDTH,
            height: TICKET_HEIGHT,
            position: { right: TICKET_POS_X, bottom: TICKET_POS_Y },
        }}
        uiBackground={{
            texture: {src: "materials/light_grey.png"},
            textureMode: 'stretch',
            // color:Color4.White() // !CF yyy enabling this helps keep ui_2 black !?  But Black or transparent hurts the texture image
        }}
    >
        <UiEntity // Caption at top of controls
            uiTransform={{ 
                positionType: 'absolute',
                width: CAPTION_WIDTH, 
                height: CAPTION_HEIGHT,
                position: { right: CAPTION_X, top: CAPTION_Y }
            }} >
            <Label
                value = "This is Ui_1 (responsive)"
                color={Color4.Black()}
                fontSize={CAPTION_FONT_SIZE}
                font="sans-serif"
                textAlign="middle-center" 
            />
        </UiEntity>
        <UiEntity // Instructions
            uiTransform={{ 
                positionType: 'absolute',
                width: INSTRUCTIONS_WIDTH, 
                height: INSTRUCTIONS_HEIGHT,
                position: { right: INSTRUCTIONS_X, top: INSTRUCTIONS_Y }
            }} >
            <Label
                value="Press Close"
                color={Color4.Black()}
                fontSize={INSTRUCTIONS_FONT_SIZE}
                font="sans-serif"
                textAlign="middle-center" 
            />
        </UiEntity>
        <Button // Close
            value='Close'
            fontSize={BUTTON_FONT_SIZE}
            font="sans-serif"
            textAlign="middle-center" 
            onMouseDown={()=>{ 
                // theTicketUi.submit()
                ui_1_submit() 
            }}
            color={Color4.Black()} // to hide the text 
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: SUBMIT_BUTTON_X, bottom: BUTTON_Y }
            }}
            uiBackground={{
                color:Color4.White(),
                texture: {src: "materials/white_button.png"}, 
                textureMode:'stretch'
            }}
        ></Button>
    </UiEntity>
}

export function showUi_1() {
    if (!ui_1_initialized) {
        ui_1_initialized = true
    }

    ui_1_id = addUiComponent(renderUi_1)
}
export function hideUi_1(){
    removeUiComponent(ui_1_id)
    ui_1_id = 0
}

function ui_1_submit() {
    hideUi_1()
}

/////////////////////////////////////////////////////////////////////////////
// Ui_2 is an example of a non-responsive, bimodel (minimized/maximized) UI
// It uses remove one and add the other to swap modes
/////////////////////////////////////////////////////////////////////////////
type Ui_2_Mode = 'maximized'|'minimized'

let ui_2_Mode:Ui_2_Mode

let ui_2_id:number = 0

function renderUi_2_Maximized() {
    const MAXIMIZED_WIDTH = 242
    const MAXIMIZED_HEIGHT = 334
    const MAXIMIZED_POS_X = 0
    const MAXIMIZED_POS_Y = 199
    
    const BUTTON_WIDTH = 29
    const BUTTON_HEIGHT = 29
    
    const COL6_X = 38 // fullscreen button
    const COL7_X = 5 // toggle uimode button
    
    const ROW0_Y = 295 // Caption at top of controls
    const ROW5_Y = 17  // Main controls row
    const ROW6_Y = 1   // Label text at bottom of controls
    
    const MAXIMIZED_CAPTION_X = 10
    const MAXIMIZED_CAPTION_WIDTH = 100
    const MAXIMIZED_CAPTION_HEIGHT = 25
    const CAPTION_FONT_SIZE = 18
    
    const MAXIMIZED_LABEL_X = 70
    const MAXIMIZED_LABEL_WIDTH = 23
    const MAXIMIZED_LABEL_HEIGHT = 18
    
    const LABEL_FONT_SIZE = 9

    return <UiEntity //parent / modal decoration
        uiTransform={{
            positionType: 'absolute',
            width: MAXIMIZED_WIDTH,
            height: MAXIMIZED_HEIGHT,
            position: { right: MAXIMIZED_POS_X, bottom: MAXIMIZED_POS_Y },
        }}
        uiBackground={{
            color:Color4.Black()
            // texture: {src: "materials/black.png"},
            // textureMode: 'stretch' 
        }}
    >
        <UiEntity // Caption at top of controls
            uiTransform={{ 
                positionType: 'absolute',
                width: MAXIMIZED_CAPTION_WIDTH, 
                height: MAXIMIZED_CAPTION_HEIGHT,
                position: { right: MAXIMIZED_CAPTION_X, bottom: ROW0_Y }
            }} >
            <Label
                value="Ui_2"
                color={Color4.White()}
                fontSize={CAPTION_FONT_SIZE}
                font="sans-serif"
                textAlign="middle-center" 
            />
        </UiEntity>
            
        <Button // Fullscreen
            value=''
            onMouseDown={()=>{ 
                displayAnnouncement("Example of displayAnnouncement\n(mouse blocker)")
            }}
            color={Color4.fromHexString("#FFFFFF00")} // to hide the text
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL6_X, bottom: ROW5_Y }
            }}
            uiBackground={{
                color:Color4.White(),
                texture: {src: "materials/expand-corners.png"}, 
                textureMode:'stretch'
            }}
        ></Button>
        <Button 
            value='ToggleMode' // using text only as documentation (button label)here.
            onMouseDown={()=>{ 
                // switch the SDK7 UI to icon mode
                ui_2_Mode = 'minimized'; 
                removeUiComponent(ui_2_id);
                ui_2_id = addUiComponent(renderUi_2_Minimized) 
            }}
            color={Color4.fromHexString("#FFFFFF00")} // to hide the text
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL7_X, bottom: ROW5_Y }
            }}
            uiBackground={{
                color:Color4.White(),
                texture: {src: "materials/expand.png"}, 
                textureMode:'stretch'
            }}
        ></Button>
        <UiEntity // bottom label
            uiTransform={{ 
                positionType: 'absolute',
                width: MAXIMIZED_LABEL_WIDTH, 
                height: MAXIMIZED_LABEL_HEIGHT,
                position: { right: MAXIMIZED_LABEL_X, bottom: ROW6_Y } 
            }} >
            <Label
                value="Ui_2 Maximized"
                color={Color4.White()}
                fontSize={LABEL_FONT_SIZE}
                font="sans-serif"
                textAlign="middle-center"
            />
        </UiEntity>
    </UiEntity>
   
}

function renderUi_2_Minimized() {
    const MINIMIZED_WIDTH = 40
    const MINIMIZED_HEIGHT = 52
    const MINIMIZED_POS_X = 0
    const MINIMIZED_POS_Y = 199

    const BUTTON_WIDTH = 29
    const BUTTON_HEIGHT = 29

    const COL7_X = 5 // toggle uimode button

    const ROW5_Y = 17  // Main controls row
    const ROW6_Y = 1   // Label text at bottom of controls

    const MINIMIZED_LABEL_X = 0
    const MINIMIZED_LABEL_WIDTH = 23
    const MINIMIZED_LABEL_HEIGHT = 18

    return <UiEntity //parent / modal decoration
        uiTransform={{
            positionType: 'absolute',        
            width: MINIMIZED_WIDTH,
            height: MINIMIZED_HEIGHT,
            position: { right: MINIMIZED_POS_X, bottom: MINIMIZED_POS_Y },
        }}
        uiBackground={{
            color:Color4.Black()
            // texture: {src: "materials/black.png"},
            // textureMode: 'stretch' 
        }}
    >
        <Button 
            value='ToggleMode' // using text only as documentation (button label)here.
            onMouseDown={()=>{ 
                // switch the SDK7 UI to full mode
                ui_2_Mode = 'maximized'
                removeUiComponent(ui_2_id);
                ui_2_id = addUiComponent(renderUi_2_Maximized) 
            }}
            color={Color4.fromHexString("#FFFFFF00")} // to hide the text
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL7_X, bottom: ROW5_Y }
            }}
            uiBackground={{
                color:Color4.White(),
                texture: {src: "materials/expand.png"}, 
                textureMode:'stretch'
            }}
        ></Button>
        <UiEntity 
            uiTransform={{ 
                positionType: 'absolute',
                width: MINIMIZED_LABEL_WIDTH, 
                height: MINIMIZED_LABEL_HEIGHT,
                position: { right: MINIMIZED_LABEL_X, bottom: ROW6_Y } 
            }} >
            <Label
                value= "Ui_2"
                color={Color4.White()}
                fontSize={9}
                font="sans-serif"
                textAlign="middle-center" 
            />
        </UiEntity>        
    </UiEntity>
}

export function showUi_2() {
    if (ui_2_id==0) {
        ui_2_Mode = 'minimized'
        ui_2_id = addUiComponent(renderUi_2_Minimized)
    }
}

export function hideUi_2(){
    if (ui_2_id > 0 ) {
        removeUiComponent(ui_2_id)
        ui_2_id = 0
    }

}

/////////////////////////////////////////////////////////////////////////////
// Ui_3 is an example of a non-responsive, bimodel (minimized/maximized) UI
// It uses a 'parent', primary renderer, 
// that heeds a Mode switch to determine which of min/max renderer to use
/////////////////////////////////////////////////////////////////////////////

type Ui_3_Mode = 'maximized'|'minimized'
let ui_3_Mode:Ui_3_Mode = 'minimized'
let ui_3_id:number = 0

function renderUi_3_Maximized() {
    const MAXIMIZED_WIDTH = 242
    const MAXIMIZED_HEIGHT = 334
    const MAXIMIZED_POS_X = 0
    const MAXIMIZED_POS_Y = 299
    
    const BUTTON_WIDTH = 29
    const BUTTON_HEIGHT = 29
    
    const COL6_X = 38 // fullscreen button
    const COL7_X = 5 // toggle uimode button
    
    const ROW0_Y = 295 // Caption at top of controls
    const ROW5_Y = 17  // Main controls row
    const ROW6_Y = 1   // Label text at bottom of controls
    
    const MAXIMIZED_CAPTION_X = 10
    const MAXIMIZED_CAPTION_WIDTH = 100
    const MAXIMIZED_CAPTION_HEIGHT = 25
    const CAPTION_FONT_SIZE = 18
    
    const MAXIMIZED_LABEL_X = 70
    const MAXIMIZED_LABEL_WIDTH = 23
    const MAXIMIZED_LABEL_HEIGHT = 18
    
    const LABEL_FONT_SIZE = 9

    return <UiEntity //parent / modal decoration
        uiTransform={{
            positionType: 'absolute',
            width: MAXIMIZED_WIDTH,
            height: MAXIMIZED_HEIGHT,
            position: { right: MAXIMIZED_POS_X, bottom: MAXIMIZED_POS_Y },
        }}
        uiBackground={{
            color:Color4.Black()
            // texture: {src: "materials/black.png"},
            // textureMode: 'stretch' 
        }}
    >
        <UiEntity // Caption at top of controls
            uiTransform={{ 
                positionType: 'absolute',
                width: MAXIMIZED_CAPTION_WIDTH, 
                height: MAXIMIZED_CAPTION_HEIGHT,
                position: { right: MAXIMIZED_CAPTION_X, bottom: ROW0_Y }
            }} >
            <Label
                value="Ui_3"
                color={Color4.White()}
                fontSize={CAPTION_FONT_SIZE}
                font="sans-serif"
                textAlign="middle-center" 
            />
        </UiEntity>
            
        <Button // Fullscreen
            value=''
            onMouseDown={()=>{ 
                // !CF note that a non-blocking displayAnnouncement doesnt show (probably occurs offscreen)
                displayAnnouncement("Example of displayAnnouncement\n(not-mouse-blocking)", 6, Color4.Yellow(), 24, false)
            }}
            color={Color4.fromHexString("#FFFFFF00")} // to hide the text
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL6_X, bottom: ROW5_Y }
            }}
            uiBackground={{
                color:Color4.White(),
                texture: {src: "materials/expand-corners.png"}, 
                textureMode:'stretch'
            }}
        ></Button>
        <Button 
            value='ToggleMode' // using text only as documentation (button label)here.
            onMouseDown={()=>{ 
                // switch the SDK7 UI to icon mode
                ui_3_Mode = 'minimized'; 
            }}
            color={Color4.fromHexString("#FFFFFF00")} // to hide the text
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL7_X, bottom: ROW5_Y }
            }}
            uiBackground={{
                color:Color4.White(),
                texture: {src: "materials/expand.png"}, 
                textureMode:'stretch'
            }}
        ></Button>
        <UiEntity // bottom label
            uiTransform={{ 
                positionType: 'absolute',
                width: MAXIMIZED_LABEL_WIDTH, 
                height: MAXIMIZED_LABEL_HEIGHT,
                position: { right: MAXIMIZED_LABEL_X, bottom: ROW6_Y } 
            }} >
            <Label
                value="Ui_3 Maximized"
                color={Color4.White()}
                fontSize={LABEL_FONT_SIZE}
                font="sans-serif"
                textAlign="middle-center"
            />
        </UiEntity>
    </UiEntity>
   
}

function renderUi_3_Minimized() {
    const MINIMIZED_WIDTH = 40
    const MINIMIZED_HEIGHT = 52
    const MINIMIZED_POS_X = 0
    const MINIMIZED_POS_Y = 299

    const BUTTON_WIDTH = 29
    const BUTTON_HEIGHT = 29

    const COL7_X = 5 // toggle uimode button

    const ROW5_Y = 17  // Main controls row
    const ROW6_Y = 1   // Label text at bottom of controls

    const MINIMIZED_LABEL_X = 0
    const MINIMIZED_LABEL_WIDTH = 23
    const MINIMIZED_LABEL_HEIGHT = 18

    return <UiEntity //parent / modal decoration
        uiTransform={{
            positionType: 'absolute',        
            width: MINIMIZED_WIDTH,
            height: MINIMIZED_HEIGHT,
            position: { right: MINIMIZED_POS_X, bottom: MINIMIZED_POS_Y },
        }}
        uiBackground={{
            color:Color4.Black()
            // texture: {src: "materials/black.png"},
            // textureMode: 'stretch' 
        }}
    >
        <Button 
            value='ToggleMode' // using text only as documentation (button label)here.
            onMouseDown={()=>{ 
                // switch the SDK7 UI to full mode
                ui_3_Mode = 'maximized'; 
            }}
            color={Color4.fromHexString("#FFFFFF00")} // to hide the text
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL7_X, bottom: ROW5_Y }
            }}
            uiBackground={{
                color:Color4.White(),
                texture: {src: "materials/expand.png"}, 
                textureMode:'stretch'
            }}
        ></Button>
        <UiEntity 
            uiTransform={{ 
                positionType: 'absolute',
                width: MINIMIZED_LABEL_WIDTH, 
                height: MINIMIZED_LABEL_HEIGHT,
                position: { right: MINIMIZED_LABEL_X, bottom: ROW6_Y } 
            }} >
            <Label
                value= "Ui_3"
                color={Color4.White()}
                fontSize={9}
                font="sans-serif"
                textAlign="middle-center" 
            />
        </UiEntity>        
    </UiEntity>
}


function renderUi_3 (){
    switch (ui_3_Mode) {
        default:
        case 'minimized':
            return renderUi_3_Minimized()
        case 'maximized':
            return renderUi_3_Maximized()
    }
}

export function showUi_3() {
    if (ui_3_id==0) {
        ui_3_id = addUiComponent(renderUi_3)
    }
}

export function hideUi_3(){
    if (ui_3_id > 0 ) {
        removeUiComponent(ui_3_id)
        ui_3_id = 0
    }

}

//////////////////////////////////
// Brief Announcements
// At the default fonsize of 32,
// it can handle up to 4 lines of text
// with about 46 characters per line 
//////////////////////////////////

class AnnouncementParams {
    constructor(
        public text:string,
        public duration:number,
        public color:Color4,
        public size:number,
        public blocking:boolean,
    ) {}

}

let announcementParams:AnnouncementParams
let announcmentUiId:number = 0
let defaultAnnouncementText =
'123456789012345678901234567890123456789012345678\n\
abcdefghijklmnopqrstuvwxyz\n\
ABCDEFGHIJKLMNOPQRSTUVWCYZ\n\
Provide at least a text param to displayAnnouncement'

export function renderAnnouncement(){
    // This is so we can while testing  this Ui rendering function 
    // directly call addUiComponent(renderAnnouncement),
    // without calling displayAnnouncement
    if (announcementParams == null) {
        announcementParams = {
            text:defaultAnnouncementText,
            duration:10,
            color:Color4.Yellow(), 
            size:20,
            blocking:true
        }
    }

    let canvas = UiCanvasInformation.get(engine.RootEntity)
    let PANEL_WIDTH = canvas.width * 0.6667 // 2/3 of the screen width
    let PANEL_HEIGHT = canvas.height * 0.333 // 1/5 of the screen height
    let PANEL_LEFT = canvas.width * 0.16667 // 1/6 of the screen to center it
    let PANEL_TOP = canvas.height * 0.333 // to center the panel vertically
    return <UiEntity //parent
        uiTransform={{
            positionType: 'absolute',        
            width: PANEL_WIDTH,
            height: PANEL_HEIGHT,
            position: { 
                left: PANEL_LEFT,
                top: PANEL_TOP },
        }}
        uiBackground={{
            color:{r:0,g:0,b:0,a:0.5}
        }}
    >
        <UiEntity
            uiTransform={{ 
                positionType: 'absolute',
                width: 0, 
                height: 0,
                position: { left: PANEL_WIDTH/2, top: PANEL_HEIGHT/2 } // centered from a 0x0 parent point that is itself centered in the panel
            }}
        >
            <Label
                value={announcementParams.text}
                color={announcementParams.color}
                fontSize={announcementParams.size}
                font="sans-serif"
                textAlign="middle-center" 
            />
        </UiEntity>
    </UiEntity>             
}

function removeAnnouncement(){
    if (debug) console.log("removeAnnouncment announcmentUiId = "+announcmentUiId)
    removeUiComponent(announcmentUiId)
    announcmentUiId = 0
}

export function displayAnnouncement(
        text:string = defaultAnnouncementText,
        duration:number=3,
        color:Color4=Color4.Yellow(),
        size:number=32,
        blocking:boolean=true)
    {
    if (announcmentUiId == 0) { // don't allow multiple simultaneous announcements 
        announcementParams = new AnnouncementParams( 
            text,
            duration,
            color,
            size,
            blocking
        )
        //!CF Announcment for some reason needs the "wrapping" entity to be full screen
        // which is how blocker blocks the screen from mouse 3d interactions,
        // but also means Announcement will appear on-screen.
        announcmentUiId = addUiComponent(renderAnnouncement, blocking)
        utils.timers.setTimeout(removeAnnouncement, 3000)
    }
}


//////////////////////////////////////////////////////
// Composite Ui manager
// Enables a scene to have multiple, unrelated Uis 
///////////////////////////////////////////////////////

const MAX_NUM_UIS = 4

type ManagedUi = {
    id:number
    // func:()=>{}
    // func:Function
    // func:()=>any
    func: ()=>ReactEcs.JSX.Element
    visible:boolean
    blocker:boolean // this forces the wrapper UiEntity to be fullscreen rather than 0 size
                    // this disables access to 3d interactions with the mouse while a blocker UI is shown.
                    // !CF this also seems necessary with displayAnnouncement to make it be onscreen.
}

let managedUis:ManagedUi[]=[]

let latestUiComponentId = 0
let UiMgrInitialized:boolean = false
let numBlockers:number = 0

let dumpUiEntities = true

// !CF
// This renderCompositUi function renders all the Uis that have been added
// using addUiComponent (but have not been removed using removeUiComponent)
// It consists of a wrapper UiEnity, and inside that are all the individual Uis rendered

// The first 4 inner entity rendering lines below render any Uis that have been added.
// this approach is more generalizable (MAX_NUM_UIS and the number of linescan could be increased),
// but it exhibits the issue of Ui_1 impacting the next-rendered of Ui_2 or Ui_3

// The later 6 inner entity rendering lines are less generalizable (must be individually coded), 
// but do not exhibit the issue of Ui_1 impacting the next-rendered of Ui_2 or Ui_3

// list version wraps a zerosized, bottom-right entity around the others
let renderCompositeUi = () => {
    // let uis:any[]|null = null
    let canvas = UiCanvasInformation.get(engine.RootEntity)
        if (dumpUiEntities) {
            if (debug) console.log("\n*** loop-compositing managedUi:")
            for (let ui of managedUis) {
                if (debug) console.log("\n id:"+ui.id+"\n visible:"+ui.visible+"\n func:"+ui.func+"\n")
            }
        }
        // !CF Why is it necessary to have the invisible "wrapper" UiEntity be full screen for displayAnnouncement to be onscreen.
        let output = <UiEntity
            uiTransform={{
                positionType: 'absolute',
                width: numBlockers>0?canvas.width:0,
                height: numBlockers>0?canvas.height:0,
                position: { right: 0, bottom: 0 },
            }}
        >

            {(useGeneralizedCompositing && (managedUis.length>0) && managedUis[0].visible)?managedUis[0].func():null}
            {(useGeneralizedCompositing && (managedUis.length>1) && managedUis[1].visible)?managedUis[1].func():null} 
            {(useGeneralizedCompositing && (managedUis.length>2) && managedUis[2].visible)?managedUis[2].func():null} 
            {(useGeneralizedCompositing && (managedUis.length>3) && managedUis[3].visible)?managedUis[3].func():null} 

            {!useGeneralizedCompositing && (ui_1_id>0)?renderUi_1():null}
            {!useGeneralizedCompositing && (ui_2_id>0 && ui_2_Mode=='minimized')?renderUi_2_Minimized():null}
            {!useGeneralizedCompositing && (ui_2_id>0 && ui_2_Mode=='maximized')?renderUi_2_Maximized():null}
            {!useGeneralizedCompositing && (ui_3_id>0 && ui_3_Mode=='minimized')?renderUi_3_Minimized():null}
            {!useGeneralizedCompositing && (ui_3_id>0 && ui_3_Mode=='maximized')?renderUi_3_Maximized():null}
            {!useGeneralizedCompositing && (announcmentUiId > 0)?renderAnnouncement():null}

        </UiEntity>
        if (dumpUiEntities) if (debug) console.log("\n*** dump ui =\n"+JSON.stringify(output))
        dumpUiEntities = false
        return output

}

export function addUiComponent(f: ()=>ReactEcs.JSX.Element, blocker:boolean=false):number {

    if ((managedUis.length >= MAX_NUM_UIS)) {
        if (debug) console.log("\n*** UiMgr.addUiComponent: too many UIs, max is "+MAX_NUM_UIS)
        return 0
    }
    latestUiComponentId += 1
    if (debug) console.log("A: latestUiComponentId="+latestUiComponentId)
    managedUis.push({
        id:latestUiComponentId,
        func:f,
        visible:true,
        blocker:blocker
    })
    if (blocker) numBlockers++
    if (!UiMgrInitialized) {
        ReactEcsRenderer.setUiRenderer(renderCompositeUi)
        UiMgrInitialized = true
    }
    // if (debug) 
        console.log("\n*** UiMgr.addUiComponent: # uis: "+managedUis.length+" #blockers: ",numBlockers)

    // if (debug) console.log("\n*** managedUis = "+managedUis)
    dumpUiEntities = true
    return latestUiComponentId
}

export function showUiComponent  (uiComponentId:number) {
    for (let i in managedUis) {
        if (managedUis[i].id == uiComponentId) {
            if (managedUis[i].blocker) numBlockers++
            managedUis[i].visible=true
        }
    }
    if (debug) console.log("\n*** UiMgr.showUiComponent: # uis: "+managedUis.length)
    dumpUiEntities = true
}

export function hideUiComponent (uiComponentId:number) {
    for (let i in managedUis) {
        if (managedUis[i].id == uiComponentId) {
            if (managedUis[i].blocker) numBlockers--
            managedUis[i].visible=false
        }
    }
    if (debug) console.log("\n*** UiMgr.hideUiComponent: # uis: "+managedUis.length, "#blockers: "+numBlockers)
    dumpUiEntities = true
}

export function removeUiComponent(uiComponentId:number):void {
    let newUiList:ManagedUi[]=[]
    for (let i in managedUis) {
        if (managedUis[i].id == uiComponentId) {
            if (managedUis[i].blocker) numBlockers--
        }
        else {
            newUiList.push(managedUis[i])
        }
    }
    managedUis = newUiList // quick atomic swap
    // if (debug) 
        console.log("\n*** UiMgr.removeUiComponent: # uis: "+managedUis.length+" #blockers: ",numBlockers)
    dumpUiEntities = true
}
